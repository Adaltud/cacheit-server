package de.cacheit.server.utilities;

import de.cacheit.server.model.BaseModel;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Currency;
import java.util.Random;
import java.util.UUID;

/**
 * Creates instances with random values
 */
@SuppressWarnings("SameParameterValue")
public class InstanceHelper {

    /* ######### HELPERS ####### */
    private static String rndString() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    private static int rndInt(int min, int max)
    {
        return java.util.concurrent.ThreadLocalRandom.current().nextInt(min, max);
    }

    private static long rndLong(long min, long max)
    {
        return java.util.concurrent.ThreadLocalRandom.current().nextLong(min, max);
    }

    private static double rndDouble(double min, double max)
    {
        return java.util.concurrent.ThreadLocalRandom.current().nextDouble(min, max);
    }

    private static String rndEmail()
    {
        return rndString() + "@example.com";
    }

    private static String rndCountry()
    {
        String[] name = {"Germany", "England", "USA", "France", "Netherlands", "Belgium", "Denmark", "Swiss", "Canada"};
        return name[new Random().nextInt(name.length)];
    }

    private static Currency rndCurrency()
    {
        String[] name = {"EUR", "CHF", "CNY", "PLN", "CZK", "RUB", "USD"};
        return Currency.getInstance(name[new Random().nextInt(name.length)]);
    }

    private static String rndFirstname(Boolean female)
    {
        String[] maleName = {"Jan", "Lukas", "Frieder", "Tim", "Tom", "Peter", "Niclas", "Danny", "Kevin", "Noel", "Hans",
                        "Marcel", "Jonas"};
        String[] femaleName = {"Franziska", "Jenny", "Petra", "Ines", "Manuela", "Olga", "Kim", "Nadia"};
        return female ? femaleName[new Random().nextInt(femaleName.length)] : maleName[new Random().nextInt(maleName.length)];
    }

    private static String rndLastname()
    {
        String[] name = {"Vogel", "Müller", "Schmidt", "Ullmann", "Mustermann", "Hecht", "Schröder", "Fischer", "Schneider"};
        return name[new Random().nextInt(name.length)];
    }

    private static LocalDate rndLocalDate(int minyear, int maxyear)
    {
        return LocalDate.of(rndInt(minyear, maxyear), rndInt(1,12), rndInt(1, 28));
    }
    private static LocalDate rndLocalDate(int minyear, int maxyear, int minmonth, int maxmonth)
    {
        return LocalDate.of(rndInt(minyear, maxyear), rndInt(minmonth, maxmonth), rndInt(1, 28));
    }
    private static String rndPlaceTitle()
    {
        String[] title = {"Erfurter Dom", "Kölner Dom", "Brandenburger Tor", "Berlin Mitte", "FH-Erfurt", "Uni Erfurt", "Veste Coburg", "Nürnberger Flughafen", "Erfurter HBF", "Münchner HBF", "Berlin HBF", "Berlin Westbahnhof"};
        return title[new Random().nextInt(title.length)];
    }

    private static <T extends BaseModel> T initId(T inst, Long value)  {
        try {
            Method privMeth = inst.getClass().getSuperclass().getDeclaredMethod("setId", Long.class);
            privMeth.setAccessible(true);
            privMeth.invoke(inst, value);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return inst;
    }

    public static <T extends BaseModel> T rndId(T inst)
    {
        return initId(inst, rndLong(1,99999));
    }

    public static <T extends BaseModel> T clearId(T inst)
    {
        return initId(inst, null);
    }

    public static <T extends BaseModel> T definedId(T inst,Long value)
    {
        return initId(inst, value);
    }
}

