package de.cacheit.server.model;

import de.cacheit.server.model.route.Route;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "PERSON")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(sequenceName = "seq_gen_person", name = "seq_gen_base", initialValue = 1000001)
public class Person extends BaseModel {

    @Column(nullable = false)
    private String userName;

    @Column(nullable = false)
    private String eMail;

    @Column(nullable = false)
    private String password;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Feedback> postedFeedbacks;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Route> solvedRoutes;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Route> savedRoutes;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Person> friends;
}
