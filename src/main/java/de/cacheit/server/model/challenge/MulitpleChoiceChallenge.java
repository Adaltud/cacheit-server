package de.cacheit.server.model.challenge;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "CHALLENGE_MULTIPLECHOICE")
@Getter
@Setter
@NoArgsConstructor
public class MulitpleChoiceChallenge extends Challenge {

    @ElementCollection
    List<String> answers;
}
