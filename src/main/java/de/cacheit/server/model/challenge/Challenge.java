package de.cacheit.server.model.challenge;

import de.cacheit.server.model.BaseModel;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;

@Entity(name = "challenge")
@Getter
@Setter(AccessLevel.PRIVATE)
@SequenceGenerator(sequenceName = "seq_gen_challenge", name = "seq_gen_base", initialValue = 1000001)
public abstract class Challenge extends BaseModel {

    @Column(nullable = false)
    String quizz;

    @Column(nullable = false)
    String correctAnswer;
}
