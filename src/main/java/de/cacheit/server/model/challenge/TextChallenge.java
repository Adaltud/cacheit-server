package de.cacheit.server.model.challenge;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CHALLENGE_TEXT")
@Getter
@Setter
@NoArgsConstructor
public class TextChallenge extends Challenge {

}
