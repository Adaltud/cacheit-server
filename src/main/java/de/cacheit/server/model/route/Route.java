package de.cacheit.server.model.route;


import de.cacheit.server.model.BaseModel;
import de.cacheit.server.model.Feedback;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "ROUTE")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(sequenceName = "seq_gen_route", name = "seq_gen_base", initialValue = 1000001)
public class Route extends BaseModel {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String description;

    private float rating = 0;

    private int solvedCounter = 0;

    private int viewCounter = 0;

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    private RoutePart startRoutePart;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<RoutePart> RoutePart;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Feedback> Feedback;
}
