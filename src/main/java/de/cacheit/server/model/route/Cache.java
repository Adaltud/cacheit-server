package de.cacheit.server.model.route;

import de.cacheit.server.model.BaseModel;
import de.cacheit.server.model.Feedback;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "CACHE")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(sequenceName = "seq_gen_cache", name = "seq_gen_base", initialValue = 1000001)
public class Cache extends BaseModel {
    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String description;

    private String NFCCode;

    @OneToOne
    private Coordinates coordinates;
}
