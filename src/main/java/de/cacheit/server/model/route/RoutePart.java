package de.cacheit.server.model.route;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.cacheit.server.model.BaseModel;
import de.cacheit.server.model.Feedback;
import de.cacheit.server.model.challenge.Challenge;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "ROUTEPART")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(sequenceName = "seq_gen_routepart", name = "seq_gen_base", initialValue = 1000001)
public class RoutePart extends BaseModel {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String description;

    @ManyToOne
    private Challenge challenge;

    @ManyToOne
    private Cache cacheToUnlock;

    @ManyToOne
    private Cache cacheToFinishPart;

    @Column(nullable = false)
    private Boolean completed;

}
