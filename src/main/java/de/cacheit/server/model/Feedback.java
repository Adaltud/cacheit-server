package de.cacheit.server.model;

import de.cacheit.server.model.route.Route;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "FEEDBACK")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(sequenceName = "seq_gen_feedback", name = "seq_gen_base", initialValue = 1000001)
public class Feedback extends BaseModel {

    @ManyToOne
    private Person user;

    @ManyToOne
    private Route route;

    @Column(nullable = false)
    private String feedback = "";

    @Column(nullable = false)
    private int rating;
}