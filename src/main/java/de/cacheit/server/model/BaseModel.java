package de.cacheit.server.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@MappedSuperclass
@Getter
@Setter(AccessLevel.PRIVATE)
public abstract class BaseModel {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_gen_base")
    private Long id;

    @Column(nullable = false)
    private Long created;

    @Column(nullable = false)
    private Long modified;

    @PrePersist
    void onCreate(){
        this.setCreated(System.currentTimeMillis());
        this.setModified(this.getCreated());
    }

    @PreUpdate
    void onUpdate(){
        this.setModified(System.currentTimeMillis());
    }
}
