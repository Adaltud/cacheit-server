package de.cacheit.server.controller.v1.api.route.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Given cache not found")
public class RoutePartNotFoundAPIException extends RuntimeException {
    public RoutePartNotFoundAPIException() {
        super();
    }

    public RoutePartNotFoundAPIException(String message) {
        super(message);
    }

    public RoutePartNotFoundAPIException(String message, Throwable cause) {
        super(message, cause);
    }
}
