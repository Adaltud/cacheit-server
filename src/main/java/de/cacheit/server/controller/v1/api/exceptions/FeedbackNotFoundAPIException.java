package de.cacheit.server.controller.v1.api.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Given feedback not found")
public class FeedbackNotFoundAPIException extends RuntimeException {
    public FeedbackNotFoundAPIException() {
        super();
    }

    public FeedbackNotFoundAPIException(String message) {
        super(message);
    }

    public FeedbackNotFoundAPIException(String message, Throwable cause) {
        super(message, cause);
    }
}
