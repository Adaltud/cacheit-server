package de.cacheit.server.controller.v1.api.challenge.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Given feedback not found")
public class ChallengeNotFoundAPIException extends RuntimeException {
    public ChallengeNotFoundAPIException() {
        super();
    }

    public ChallengeNotFoundAPIException(String message) {
        super(message);
    }

    public ChallengeNotFoundAPIException(String message, Throwable cause) {
        super(message, cause);
    }
}
