package de.cacheit.server.controller.v1.api;

import de.cacheit.server.controller.v1.api.exceptions.FeedbackNotFoundAPIException;
import de.cacheit.server.controller.v1.api.exceptions.IdMismatchAPIException;
import de.cacheit.server.controller.v1.api.exceptions.MissingValuesAPIException;
import de.cacheit.server.model.Feedback;
import de.cacheit.server.model.Feedback;
import de.cacheit.server.model.QPerson;
import de.cacheit.server.storage.repositories.FeedbackRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static de.cacheit.server.model.QFeedback.feedback1;


@RestController
@RequestMapping("api/v1/feedback")
public class FeedbackController extends BaseController<Feedback> {

    FeedbackRepo repo;
    private static final Logger LOG = LoggerFactory.getLogger(FeedbackController.class);

    @Autowired
    public FeedbackController(FeedbackRepo repo) {
        this.type = Feedback.class;
        this.repo = repo;
    }

    private Feedback fetchFeedback(Long feedbackId) throws FeedbackNotFoundAPIException {
        LOG.info("Find feedback: " + feedbackId);

        Optional<Feedback> feedback = repo.findById(feedbackId);

        if (!feedback.isPresent())
            throw new FeedbackNotFoundAPIException();

        LOG.info("Feedback found: " + feedbackId);
        return feedback.get();
    }

    //<editor-fold desc="CRUD">
    //###################
    //##### CREATE ######
    //###################

    /**
     * Create a new feedback
     * @param feedback The feedback to create
     * @return The saved feedback
     */
    @PostMapping("")
    @ResponseStatus(code = HttpStatus.CREATED)
    public Feedback createFeedback(@RequestBody Feedback feedback) {
        LOG.info("Save feedback...");

        Feedback p = repo.save(feedback);

        LOG.info("Feedback saved..." + feedback.toString());

        return p;
    }

    //###################
    //###### READ #######
    //###################

    /**
     * Read an existing feedback
     * @param feedbackId The feedback to read
     * @return The found feedback
     * @throws FeedbackNotFoundAPIException If the feedback was not found
     */
    @GetMapping("/{feedbackId}")
    @ResponseStatus(code = HttpStatus.OK)
    public Feedback getFeedback(@PathVariable Long feedbackId) throws FeedbackNotFoundAPIException {
        return fetchFeedback(feedbackId);
    }

    /**
     * Read all existing feedbacks
     * @return The found feedbacks
     */
    @GetMapping("")
    @ResponseStatus(code = HttpStatus.OK)
    public List<Feedback> getFeedbacks() {
        return toListT(repo.findAll());
    }

    /**
     * Find feedbacks based on an search string
     * Name, firstname of the user and the feedbackid are considered
     * @param searchQ The journey to read
     * @return The found feedbacks
     * @throws FeedbackNotFoundAPIException If no feedback was not found
     */
    @GetMapping("/search/{searchQ}")
    @ResponseStatus(code = HttpStatus.OK)
    public List<Feedback> findJourneys(@PathVariable String searchQ) throws FeedbackNotFoundAPIException {
        LOG.info("Search feedbacks with query: " + searchQ);
        return toListT(repo.findAll(feedback1.id.stringValue().contains(searchQ)
                .or(feedback1.user.userName.contains(searchQ))
                .or(feedback1.user.eMail.contains(searchQ))));
    }

    //###################
    //##### UPDATE ######
    //###################

    /**
     * Update an existing Feedback
     * @param feedbackId The feedback to update
     * @param feedback The new feedback
     * @return The updated feedback
     */
    @PutMapping("/{feedbackId}")
    @ResponseStatus(code = HttpStatus.OK)
    public Feedback updateFeedback(@PathVariable Long feedbackId, @RequestBody Feedback feedback) {
        LOG.info("Update feedback: " + feedbackId);

        Feedback p1 = fetchFeedback(feedbackId);

        if (feedback.getId() == null)
            throw new MissingValuesAPIException("Missing values: id");

        if (!feedback.getId().equals(feedbackId))
            throw new IdMismatchAPIException(String.format("Ids %d and %d do not match.", feedbackId, feedback.getId()));

        copyNonNullProperties(feedback, p1);
        Feedback p = repo.save(p1);
        LOG.info("Feedback updated: " + feedbackId);
        return p;
    }

    //###################
    //##### DELETE ######
    //###################

    /**
     * Delete an existing feedback
     * @param feedbackId The feedback to delete
     * @throws FeedbackNotFoundAPIException If the feedback was not found
     */
    @DeleteMapping("/{feedbackId}")
    @ResponseStatus(code = HttpStatus.OK)
    void deleteFeedback(@PathVariable Long feedbackId) throws FeedbackNotFoundAPIException {
        LOG.info("Delete feedback: " + feedbackId);
        repo.delete(fetchFeedback(feedbackId));
    }
    //</editor-fold>
}
