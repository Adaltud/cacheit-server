package de.cacheit.server.controller.v1.api.route;

import de.cacheit.server.controller.v1.api.BaseController;
import de.cacheit.server.controller.v1.api.exceptions.IdMismatchAPIException;
import de.cacheit.server.controller.v1.api.exceptions.MissingValuesAPIException;
import de.cacheit.server.controller.v1.api.route.exception.RoutePartNotFoundAPIException;
import de.cacheit.server.model.route.RoutePart;
import de.cacheit.server.storage.repositories.route.RoutePartRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static de.cacheit.server.model.route.QRoutePart.routePart;


@RestController
@RequestMapping("api/v1/routepart")
public class RoutePartController extends BaseController<RoutePart> {

    RoutePartRepo repo;
    private static final Logger LOG = LoggerFactory.getLogger(RoutePartController.class);

    @Autowired
    public RoutePartController(RoutePartRepo repo) {
        this.type = RoutePart.class;
        this.repo = repo;
    }

    private RoutePart fetchRoutePart(Long routePartId) throws RoutePartNotFoundAPIException {
        LOG.info("Find routePart: " + routePartId);

        Optional<RoutePart> routePart = repo.findById(routePartId);

        if (!routePart.isPresent())
            throw new RoutePartNotFoundAPIException();

        LOG.info("RoutePart found: " + routePartId);
        return routePart.get();
    }

    //<editor-fold desc="CRUD">
    //###################
    //##### CREATE ######
    //###################

    /**
     * Create a new routePart
     * @param routePart The routePart to create
     * @return The saved routePart
     */
    @PostMapping("")
    @ResponseStatus(code = HttpStatus.CREATED)
    public RoutePart createRoutePart(@RequestBody RoutePart routePart) {
        LOG.info("Save routePart...");

        RoutePart p = repo.save(routePart);

        LOG.info("RoutePart saved..." + routePart.toString());

        return p;
    }

    //###################
    //###### READ #######
    //###################

    /**
     * Read an existing routePart
     * @param routePartId The routePart to read
     * @return The found routePart
     * @throws RoutePartNotFoundAPIException If the routePart was not found
     */
    @GetMapping("/{routePartId}")
    @ResponseStatus(code = HttpStatus.OK)
    public RoutePart getRoutePart(@PathVariable Long routePartId) throws RoutePartNotFoundAPIException {
        return fetchRoutePart(routePartId);
    }

    /**
     * Read all existing routeParts
     * @return The found routeParts
     */
    @GetMapping("")
    @ResponseStatus(code = HttpStatus.OK)
    public List<RoutePart> getRouteParts() {
        return toListT(repo.findAll());
    }

    /**
     * Find routeParts based on an search string
     * Name and id are considered
     * @param searchQ The journey to read
     * @return The found routeParts
     * @throws RoutePartNotFoundAPIException If no routePart was not found
     */
    @GetMapping("/search/{searchQ}")
    @ResponseStatus(code = HttpStatus.OK)
    public List<RoutePart> findJourneys(@PathVariable String searchQ) throws RoutePartNotFoundAPIException {
        LOG.info("Search routeParts with query: " + searchQ);
        return toListT(repo.findAll(routePart.id.stringValue().contains(searchQ)
                .or(routePart.name.contains(searchQ))
                ));
    }

    //###################
    //##### UPDATE ######
    //###################

    /**
     * Update an existing RoutePart
     * @param routePartId The routePart to update
     * @param routePart The new routePart
     * @return The updated routePart
     */
    @PutMapping("/{routePartId}")
    @ResponseStatus(code = HttpStatus.OK)
    public RoutePart updateRoutePart(@PathVariable Long routePartId, @RequestBody RoutePart routePart) {
        LOG.info("Update routePart: " + routePartId);

        RoutePart p1 = fetchRoutePart(routePartId);

        if (routePart.getId() == null)
            throw new MissingValuesAPIException("Missing values: id");

        if (!routePart.getId().equals(routePartId))
            throw new IdMismatchAPIException(String.format("Ids %d and %d do not match.", routePartId, routePart.getId()));

        copyNonNullProperties(routePart, p1);
        RoutePart p = repo.save(p1);
        LOG.info("RoutePart updated: " + routePartId);
        return p;
    }

    //###################
    //##### DELETE ######
    //###################

    /**
     * Delete an existing routePart
     * @param routePartId The routePart to delete
     * @throws RoutePartNotFoundAPIException If the routePart was not found
     */
    @DeleteMapping("/{routePartId}")
    @ResponseStatus(code = HttpStatus.OK)
    void deleteRoutePart(@PathVariable Long routePartId) throws RoutePartNotFoundAPIException {
        LOG.info("Delete routePart: " + routePartId);
        repo.delete(fetchRoutePart(routePartId));
    }
    //</editor-fold>
}
