package de.cacheit.server.controller.v1.api.challenge;

import de.cacheit.server.model.challenge.TextChallenge;
import de.cacheit.server.storage.repositories.challenge.TextChallengeRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/textchallenge")
public class TextChallengeController extends ChallengeController<TextChallenge> {

    private static final Logger LOG = LoggerFactory.getLogger(TextChallengeController.class);

    @Autowired
    public TextChallengeController(TextChallengeRepo repoAccommodation) {
        super();
        this.repoChallenge = repoAccommodation;
        this.type = TextChallenge.class;
    }
}
