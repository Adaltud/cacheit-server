package de.cacheit.server.controller.v1.api.route.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Given cache not found")
public class RouteNotFoundAPIException extends RuntimeException {
    public RouteNotFoundAPIException() {
        super();
    }

    public RouteNotFoundAPIException(String message) {
        super(message);
    }

    public RouteNotFoundAPIException(String message, Throwable cause) {
        super(message, cause);
    }
}
