package de.cacheit.server.controller.v1.api.challenge;

import de.cacheit.server.controller.v1.api.BaseController;
import de.cacheit.server.controller.v1.api.challenge.exceptions.ChallengeNotFoundAPIException;
import de.cacheit.server.controller.v1.api.exceptions.IdMismatchAPIException;
import de.cacheit.server.controller.v1.api.exceptions.MissingValuesAPIException;
import de.cacheit.server.model.challenge.Challenge;
import de.cacheit.server.storage.repositories.PersonRepo;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Component
public abstract class ChallengeController<T extends Challenge> extends BaseController<T> {
    private static final Logger LOG = LoggerFactory.getLogger(ChallengeController.class);

    @Setter
    CrudRepository<T, Long> repoChallenge;

    @Autowired
    public ChallengeController() {
    }

    private T fetchChallenge(Long challengeId) {
        LOG.info("Find challenge: " + challengeId);

        Optional<T> challenge = repoChallenge.findById(challengeId);

        if (!challenge.isPresent())
            throw new ChallengeNotFoundAPIException();

        LOG.info("Challenge found: " + challengeId);
        return challenge.get();
    }

    //<editor-fold desc="CRUD">
    //###################
    //##### CREATE ######
    //###################
    @PostMapping("")
    @ResponseStatus(code = HttpStatus.CREATED)
    public T createChallenge(@RequestBody T Challenge) {
        LOG.info("Create challenge..."+ Challenge.getId());

        T loc = repoChallenge.save(Challenge);

        LOG.debug("Challenge saved: " + loc.getId());
        return loc;
    }

    //###################
    //###### READ #######
    //###################
    @GetMapping("/{challengeId}")
    @ResponseStatus(code = HttpStatus.OK)
    public T getChallenge(@PathVariable Long challengeId) throws ChallengeNotFoundAPIException {
        LOG.info("Read challenge: " + challengeId);
        return fetchChallenge(challengeId);
    }


    @GetMapping("")
    @ResponseStatus(code = HttpStatus.OK)
    public List<T> getChallenge() throws ChallengeNotFoundAPIException {
        LOG.info("Read all challenges...");
        return toListT(repoChallenge.findAll());
    }

    //###################
    //##### UPDATE ######
    //###################
    @PutMapping("/{challengeId}")
    @ResponseStatus(code = HttpStatus.OK)
    public T updateChallenge(@PathVariable Long challengeId, @RequestBody T challenge) throws ChallengeNotFoundAPIException {
        LOG.info("Update challenge: " + challengeId);
        T loc1 = fetchChallenge(challengeId);

        if (challenge.getId() == null)
            throw new MissingValuesAPIException("Missing values: id");

        if (!challenge.getId().equals(challengeId))
            throw new IdMismatchAPIException(String.format("Ids %d and %d do not match.", challengeId, challenge.getId()));

        copyNonNullProperties(challenge, loc1);
        T loc = repoChallenge.save(loc1);
        LOG.info("Updated challenge: " + challengeId);
        return loc;
    }

    //###################
    //##### DELETE ######
    //###################
    @DeleteMapping("/{challengeId}")
    @ResponseStatus(code = HttpStatus.OK)
    void deleteChallenge(@PathVariable Long challengeId) throws ChallengeNotFoundAPIException {
        LOG.info("Delete challenge: " + challengeId);
        repoChallenge.delete(fetchChallenge(challengeId));
        LOG.info("Deleted challenge: " + challengeId);
    }
    //</editor-fold>
}
