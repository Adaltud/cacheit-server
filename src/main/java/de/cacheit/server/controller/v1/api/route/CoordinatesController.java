package de.cacheit.server.controller.v1.api.route;

import de.cacheit.server.controller.v1.api.BaseController;
import de.cacheit.server.controller.v1.api.exceptions.IdMismatchAPIException;
import de.cacheit.server.controller.v1.api.exceptions.MissingValuesAPIException;
import de.cacheit.server.controller.v1.api.route.exception.CacheNotFoundAPIException;
import de.cacheit.server.controller.v1.api.route.exception.CoordinatesNotFoundAPIException;
import de.cacheit.server.model.route.Coordinates;
import de.cacheit.server.storage.repositories.route.CoordinatesRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static de.cacheit.server.model.route.QCoordinates.coordinates;


@RestController
@RequestMapping("api/v1/coordinates")
public class CoordinatesController extends BaseController<Coordinates> {

    CoordinatesRepo repo;
    private static final Logger LOG = LoggerFactory.getLogger(CoordinatesController.class);

    @Autowired
    public CoordinatesController(CoordinatesRepo repo) {
        this.type = Coordinates.class;
        this.repo = repo;
    }

    private Coordinates fetchCoordinates(Long coordinatesId) throws CacheNotFoundAPIException {
        LOG.info("Find coordinates: " + coordinatesId);

        Optional<Coordinates> coordinates = repo.findById(coordinatesId);

        if (!coordinates.isPresent())
            throw new CacheNotFoundAPIException();

        LOG.info("Cache found: " + coordinatesId);
        return coordinates.get();
    }

    //<editor-fold desc="CRUD">
    //###################
    //##### CREATE ######
    //###################

    /**
     * Create a new coordinates
     * @param coordinates The coordinates to create
     * @return The saved coordinates
     */
    @PostMapping("")
    @ResponseStatus(code = HttpStatus.CREATED)
    public Coordinates createCache(@RequestBody Coordinates coordinates) {
        LOG.info("Save coordinates...");

        Coordinates p = repo.save(coordinates);

        LOG.info("Coordinates saved..." + coordinates.toString());

        return p;
    }

    //###################
    //###### READ #######
    //###################

    /**
     * Read an existing cache
     * @param coordinatesId The cache to read
     * @return The found cache
     * @throws CacheNotFoundAPIException If the cache was not found
     */
    @GetMapping("/{coordinatesId}")
    @ResponseStatus(code = HttpStatus.OK)
    public Coordinates getCache(@PathVariable Long coordinatesId) throws CoordinatesNotFoundAPIException {
        return fetchCoordinates(coordinatesId);
    }

    /**
     * Read all existing caches
     * @return The found caches
     */
    @GetMapping("")
    @ResponseStatus(code = HttpStatus.OK)
    public List<Coordinates> getCaches() {
        return toListT(repo.findAll());
    }

    /**
     * Find coordinates based on an search string
     * Name, description and id are considered
     * @param searchQ The coordinates to read
     * @return The found coordinates
     * @throws CacheNotFoundAPIException If no coordinates was not found
     */
    @GetMapping("/search/{searchQ}")
    @ResponseStatus(code = HttpStatus.OK)
    public List<Coordinates> findJourneys(@PathVariable String searchQ) throws CoordinatesNotFoundAPIException {
        LOG.info("Search coordinates with query: " + searchQ);
        return toListT(repo.findAll(coordinates.id.stringValue().contains(searchQ)
                .or(coordinates.id.stringValue().contains(searchQ))
                .or(coordinates.created.stringValue().contains(searchQ))));
    }

    //###################
    //##### UPDATE ######
    //###################

    /**
     * Update an existing Coordinates
     * @param coordinatesId The coordinates to update
     * @param coordinates The new coordinates
     * @return The updated cache
     */
    @PutMapping("/{cacheId}")
    @ResponseStatus(code = HttpStatus.OK)
    public Coordinates updateCoordinates(@PathVariable Long coordinatesId, @RequestBody Coordinates coordinates) {
        LOG.info("Update coordinates: " + coordinatesId);

        Coordinates p1 = fetchCoordinates(coordinatesId);

        if (coordinates.getId() == null)
            throw new MissingValuesAPIException("Missing values: id");

        if (!coordinates.getId().equals(coordinatesId))
            throw new IdMismatchAPIException(String.format("Ids %d and %d do not match.", coordinatesId, coordinates.getId()));

        copyNonNullProperties(coordinates, p1);
        Coordinates p = repo.save(p1);
        LOG.info("Cache updated: " + coordinatesId);
        return p;
    }

    //###################
    //##### DELETE ######
    //###################

    /**
     * Delete an existing coordinates
     * @param coordinatesId The coordinates to delete
     * @throws CacheNotFoundAPIException If the coordinates was not found
     */
    @DeleteMapping("/{coordinatesId}")
    @ResponseStatus(code = HttpStatus.OK)
    void deleteCache(@PathVariable Long coordinatesId) throws CoordinatesNotFoundAPIException {
        LOG.info("Delete cache: " + coordinatesId);
        repo.delete(fetchCoordinates(coordinatesId));
    }
    //</editor-fold>
}
