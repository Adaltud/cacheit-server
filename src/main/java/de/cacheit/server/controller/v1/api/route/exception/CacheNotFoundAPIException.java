package de.cacheit.server.controller.v1.api.route.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Given cache not found")
public class CacheNotFoundAPIException extends RuntimeException {
    public CacheNotFoundAPIException() {
        super();
    }

    public CacheNotFoundAPIException(String message) {
        super(message);
    }

    public CacheNotFoundAPIException(String message, Throwable cause) {
        super(message, cause);
    }
}
