package de.cacheit.server.controller.v1.api.route;

import de.cacheit.server.controller.v1.api.BaseController;
import de.cacheit.server.controller.v1.api.exceptions.IdMismatchAPIException;
import de.cacheit.server.controller.v1.api.exceptions.MissingValuesAPIException;
import de.cacheit.server.controller.v1.api.route.exception.RouteNotFoundAPIException;
import de.cacheit.server.model.route.Route;
import de.cacheit.server.storage.repositories.route.RouteRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static de.cacheit.server.model.route.QRoute.route;


@RestController
@RequestMapping("api/v1/route")
public class RouteController extends BaseController<Route> {

    RouteRepo repo;
    private static final Logger LOG = LoggerFactory.getLogger(RouteController.class);

    @Autowired
    public RouteController(RouteRepo repo) {
        this.type = Route.class;
        this.repo = repo;
    }

    private Route fetchRoute(Long routeId) throws RouteNotFoundAPIException {
        LOG.info("Find route: " + routeId);

        Optional<Route> route = repo.findById(routeId);

        if (!route.isPresent())
            throw new RouteNotFoundAPIException();

        LOG.info("Route found: " + routeId);
        return route.get();
    }

    //<editor-fold desc="CRUD">
    //###################
    //##### CREATE ######
    //###################

    /**
     * Create a new route
     * @param route The route to create
     * @return The saved route
     */
    @PostMapping("")
    @ResponseStatus(code = HttpStatus.CREATED)
    public Route createRoute(@RequestBody Route route) {
        LOG.info("Save route...");

        Route p = repo.save(route);

        LOG.info("Route saved..." + route.toString());

        return p;
    }

    //###################
    //###### READ #######
    //###################

    /**
     * Read an existing route
     * @param routeId The route to read
     * @return The found route
     * @throws RouteNotFoundAPIException If the route was not found
     */
    @GetMapping("/{routeId}")
    @ResponseStatus(code = HttpStatus.OK)
    public Route getRoute(@PathVariable Long routeId) throws RouteNotFoundAPIException {
        return fetchRoute(routeId);
    }

    /**
     * Read all existing routes
     * @return The found routes
     */
    @GetMapping("")
    @ResponseStatus(code = HttpStatus.OK)
    public List<Route> getRoutes() {
        return toListT(repo.findAll());
    }

    /**
     * Find routes based on an search string
     * Name, description and id are considered
     * @param searchQ The journey to read
     * @return The found routes
     * @throws RouteNotFoundAPIException If no route was not found
     */
    @GetMapping("/search/{searchQ}")
    @ResponseStatus(code = HttpStatus.OK)
    public List<Route> findJourneys(@PathVariable String searchQ) throws RouteNotFoundAPIException {
        LOG.info("Search routes with query: " + searchQ);
        return toListT(repo.findAll(route.id.stringValue().contains(searchQ)
                .or(route.name.contains(searchQ))
                .or(route.description.contains(searchQ))));
    }

    //###################
    //##### UPDATE ######
    //###################

    /**
     * Update an existing Route
     * @param routeId The route to update
     * @param route The new route
     * @return The updated route
     */
    @PutMapping("/{routeId}")
    @ResponseStatus(code = HttpStatus.OK)
    public Route updateRoute(@PathVariable Long routeId, @RequestBody Route route) {
        LOG.info("Update route: " + routeId);

        Route p1 = fetchRoute(routeId);

        if (route.getId() == null)
            throw new MissingValuesAPIException("Missing values: id");

        if (!route.getId().equals(routeId))
            throw new IdMismatchAPIException(String.format("Ids %d and %d do not match.", routeId, route.getId()));

        copyNonNullProperties(route, p1);
        Route p = repo.save(p1);
        LOG.info("Route updated: " + routeId);
        return p;
    }

    //###################
    //##### DELETE ######
    //###################

    /**
     * Delete an existing route
     * @param routeId The route to delete
     * @throws RouteNotFoundAPIException If the route was not found
     */
    @DeleteMapping("/{routeId}")
    @ResponseStatus(code = HttpStatus.OK)
    void deleteRoute(@PathVariable Long routeId) throws RouteNotFoundAPIException {
        LOG.info("Delete route: " + routeId);
        repo.delete(fetchRoute(routeId));
    }
    //</editor-fold>
}
