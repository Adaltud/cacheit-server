package de.cacheit.server.controller.v1.api.route;

import de.cacheit.server.controller.v1.api.BaseController;
import de.cacheit.server.controller.v1.api.route.exception.CacheNotFoundAPIException;
import de.cacheit.server.controller.v1.api.exceptions.IdMismatchAPIException;
import de.cacheit.server.controller.v1.api.exceptions.MissingValuesAPIException;
import de.cacheit.server.model.route.Cache;
import de.cacheit.server.storage.repositories.route.CacheRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static de.cacheit.server.model.route.QCache.cache;


@RestController
@RequestMapping("api/v1/cache")
public class CacheController extends BaseController<Cache> {

    CacheRepo repo;
    private static final Logger LOG = LoggerFactory.getLogger(CacheController.class);

    @Autowired
    public CacheController(CacheRepo repo) {
        this.type = Cache.class;
        this.repo = repo;
    }

    private Cache fetchCache(Long cacheId) throws CacheNotFoundAPIException {
        LOG.info("Find cache: " + cacheId);

        Optional<Cache> cache = repo.findById(cacheId);

        if (!cache.isPresent())
            throw new CacheNotFoundAPIException();

        LOG.info("Cache found: " + cacheId);
        return cache.get();
    }

    //<editor-fold desc="CRUD">
    //###################
    //##### CREATE ######
    //###################

    /**
     * Create a new cache
     * @param cache The cache to create
     * @return The saved cache
     */
    @PostMapping("")
    @ResponseStatus(code = HttpStatus.CREATED)
    public Cache createCache(@RequestBody Cache cache) {
        LOG.info("Save cache...");

        Cache p = repo.save(cache);

        LOG.info("Cache saved..." + cache.toString());

        return p;
    }

    //###################
    //###### READ #######
    //###################

    /**
     * Read an existing cache
     * @param cacheId The cache to read
     * @return The found cache
     * @throws CacheNotFoundAPIException If the cache was not found
     */
    @GetMapping("/{cacheId}")
    @ResponseStatus(code = HttpStatus.OK)
    public Cache getCache(@PathVariable Long cacheId) throws CacheNotFoundAPIException {
        return fetchCache(cacheId);
    }

    /**
     * Read all existing caches
     * @return The found caches
     */
    @GetMapping("")
    @ResponseStatus(code = HttpStatus.OK)
    public List<Cache> getCaches() {
        return toListT(repo.findAll());
    }

    /**
     * Find caches based on an search string
     * Name, description and id are considered
     * @param searchQ The journey to read
     * @return The found caches
     * @throws CacheNotFoundAPIException If no cache was not found
     */
    @GetMapping("/search/{searchQ}")
    @ResponseStatus(code = HttpStatus.OK)
    public List<Cache> findJourneys(@PathVariable String searchQ) throws CacheNotFoundAPIException {
        LOG.info("Search caches with query: " + searchQ);
        return toListT(repo.findAll(cache.id.stringValue().contains(searchQ)
                .or(cache.name.contains(searchQ))
                .or(cache.description.contains(searchQ))));
    }

    //###################
    //##### UPDATE ######
    //###################

    /**
     * Update an existing Cache
     * @param cacheId The cache to update
     * @param cache The new cache
     * @return The updated cache
     */
    @PutMapping("/{cacheId}")
    @ResponseStatus(code = HttpStatus.OK)
    public Cache updateCache(@PathVariable Long cacheId, @RequestBody Cache cache) {
        LOG.info("Update cache: " + cacheId);

        Cache p1 = fetchCache(cacheId);

        if (cache.getId() == null)
            throw new MissingValuesAPIException("Missing values: id");

        if (!cache.getId().equals(cacheId))
            throw new IdMismatchAPIException(String.format("Ids %d and %d do not match.", cacheId, cache.getId()));

        copyNonNullProperties(cache, p1);
        Cache p = repo.save(p1);
        LOG.info("Cache updated: " + cacheId);
        return p;
    }

    //###################
    //##### DELETE ######
    //###################

    /**
     * Delete an existing cache
     * @param cacheId The cache to delete
     * @throws CacheNotFoundAPIException If the cache was not found
     */
    @DeleteMapping("/{cacheId}")
    @ResponseStatus(code = HttpStatus.OK)
    void deleteCache(@PathVariable Long cacheId) throws CacheNotFoundAPIException {
        LOG.info("Delete cache: " + cacheId);
        repo.delete(fetchCache(cacheId));
    }
    //</editor-fold>
}
