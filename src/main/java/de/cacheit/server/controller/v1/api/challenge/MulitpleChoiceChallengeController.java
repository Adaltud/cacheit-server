package de.cacheit.server.controller.v1.api.challenge;

import de.cacheit.server.model.challenge.MulitpleChoiceChallenge;
import de.cacheit.server.model.challenge.TextChallenge;
import de.cacheit.server.storage.repositories.challenge.MultipleChoiceChallengeRepo;
import de.cacheit.server.storage.repositories.challenge.TextChallengeRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/multiplechoicechallenge")
public class MulitpleChoiceChallengeController extends ChallengeController<MulitpleChoiceChallenge> {

    private static final Logger LOG = LoggerFactory.getLogger(MulitpleChoiceChallengeController.class);

    @Autowired
    public MulitpleChoiceChallengeController(MultipleChoiceChallengeRepo repoAccommodation) {
        super();
        this.repoChallenge = repoAccommodation;
        this.type = MulitpleChoiceChallenge.class;
    }
}
