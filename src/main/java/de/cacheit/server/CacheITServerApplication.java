package de.cacheit.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CacheITServerApplication {

    public static void main(String[] args)  {
        SpringApplication.run(CacheITServerApplication.class, args);
    }

}
