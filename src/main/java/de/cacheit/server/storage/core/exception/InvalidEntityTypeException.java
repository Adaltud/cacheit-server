package de.cacheit.server.storage.core.exception;

public class InvalidEntityTypeException extends Throwable {
    public InvalidEntityTypeException() {
        super();
    }
    public InvalidEntityTypeException(String message) {
        super(message);
    }
}
