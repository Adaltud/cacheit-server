package de.cacheit.server.storage.repositories.route;

import de.cacheit.server.model.Person;
import de.cacheit.server.model.route.Cache;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

/**
 * Allows data access
 */

public interface CacheRepo extends CrudRepository<Cache, Long>, QuerydslPredicateExecutor<Cache> {
}

