package de.cacheit.server.storage.repositories;

import de.cacheit.server.model.Feedback;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

/**
 * Allows data access
 */

public interface FeedbackRepo extends CrudRepository<Feedback, Long>, QuerydslPredicateExecutor<Feedback> {
}

