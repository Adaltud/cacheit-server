package de.cacheit.server.storage.repositories.route;

import de.cacheit.server.model.route.Cache;
import de.cacheit.server.model.route.Route;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

/**
 * Allows data access
 */

public interface RouteRepo extends CrudRepository<Route, Long>, QuerydslPredicateExecutor<Route> {
}

