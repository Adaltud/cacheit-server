package de.cacheit.server.storage.repositories.challenge;

import de.cacheit.server.model.challenge.TextChallenge;
import de.cacheit.server.model.route.RoutePart;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

/**
 * Allows data access
 */

public interface TextChallengeRepo extends CrudRepository<TextChallenge, Long>, QuerydslPredicateExecutor<TextChallenge> {
}

