package de.cacheit.server.storage.repositories.challenge;

import de.cacheit.server.model.challenge.MulitpleChoiceChallenge;
import de.cacheit.server.model.challenge.TextChallenge;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

/**
 * Allows data access
 */

public interface MultipleChoiceChallengeRepo extends CrudRepository<MulitpleChoiceChallenge, Long>, QuerydslPredicateExecutor<MulitpleChoiceChallenge> {
}

