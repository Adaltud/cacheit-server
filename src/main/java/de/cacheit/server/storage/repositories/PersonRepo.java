package de.cacheit.server.storage.repositories;

import de.cacheit.server.model.Person;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

/**
 * Allows data access
 */

public interface PersonRepo extends CrudRepository<Person, Long>, QuerydslPredicateExecutor<Person> {
}

