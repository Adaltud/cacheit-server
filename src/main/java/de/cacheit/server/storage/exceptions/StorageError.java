package de.cacheit.server.storage.exceptions;

public class StorageError extends Throwable {
    public StorageError(String message) {
        super(message);
    }
}
